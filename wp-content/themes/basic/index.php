<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="wp-content/themes/best/style.css">
</head>

<body>
    <header>
        Le Bestiaire
    </header>
    <main>
        <h1>Index</h1>

        <div class="col">
            <h2>A</h2>
            <ul>
                <li><a href="http://localhost/bestiaire/index.php/ane">Ane</a></li>
                <li><a href="http://localhost/bestiaire/index.php/autruche">Autruche</a></li>
            </ul>
        </div>

        <div class="col">
            <h2>B</h2>
            <ul>
                <li><a href="http://localhost/bestiaire/index.php/buffle">Buffle</a></li>
                <li><a href="http://localhost/bestiaire/index.php/brebis">Brebis</a></li>
            </ul>
        </div>

        <div class="col">
            <h2>C</h2>
            <ul>
                <li><a href="http://localhost/bestiaire/index.php/crocodile">Crocodile</a></li>
                <li><a href="http://localhost/bestiaire/index.php/canard">Canard</a></li>
            </ul>
        </div>

        <div class="col">
            <h2>D</h2>
            <ul>
                <li><a href="http://localhost/bestiaire/index.php/dindon">Dindon</a></li>
                <li><a href="http://localhost/bestiaire/index.php/dromadaire">Dromadaire</a></li>
            </ul>
        </div>
    
        <div class="col">
            <h2>L</h2>
            <ul>
                <li><a href="http://localhost/bestiaire/index.php/loup">Loup</a></li>
                <li><a href="http://localhost/bestiaire/index.php/lapin">Lapin</a></li>
            </ul>
        </div>

        <div class="col">
            <h2>P</h2>
            <ul>
                <li><a href="http://localhost/bestiaire/index.php/poule">Poule</a></li>
                <li><a href="http://localhost/bestiaire/index.php/panthere">Panthère</a></li>
            </ul>
        </div>

        <div class="col">
            <h2>V</h2>
            <ul>
                <li><a href="#">Vache</a></li>
                <li><a href="#">Varan</a></li>
            </ul>
        </div>


    </main>
</body>

</html>