<?php get_header(); ?>
	<main id="content" class="content">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php do_action( 'basic_before_page_article' ); ?>
			<article class="post page" id="pageid-<?php the_ID(); ?>">
				
				<?php do_action( 'basic_before_page_title' );  ?>
				<h1><?php the_title(); ?></h1>
				<?php do_action( 'basic_after_page_title' );  ?>

				<?php do_action( 'basic_before_page_content_box' );  ?>
				<div class="entry-box clearfix">
					<?php do_action( 'basic_before_page_content' );  ?>
               <?php/* echo get_field('animal'); */?>
               <?php $image = get_field('image'); ?>
               <img src="<?php echo $image['url']; ?>"><br>
			   <?php echo "taille : ".get_field('taille'); ?>
			   <?php $array = get_field('mange'); ?>
			   <ul class="ul-bestiaire-single">
		<?php echo "régime alimentaire : " ?>
        <?php foreach($array as $mange){?> 
            <li><a class="a-bestiaire-single" href="<?php echo $mange -> guid;?>"><?php echo $mange -> post_title; }?></a></li>
        </ul>
					<?php the_content(); ?>
					<?php do_action( 'basic_after_page_content' );  ?>
				</div>
				<?php do_action( 'basic_after_page_content_box' );  ?>

			</article>
			<?php do_action( 'basic_after_page_article' ); ?>


			<?php 

			if ( comments_open() || get_comments_number() ) {
				do_action( 'basic_before_page_comments_area' );
				comments_template();
				do_action( 'basic_after_page_comments_area' );
			}

		endwhile; ?>
		
	</main> <!-- #content -->
	<?php get_sidebar(); ?>